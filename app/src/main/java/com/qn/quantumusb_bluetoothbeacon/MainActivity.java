package com.qn.quantumusb_bluetoothbeacon;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.qn.quantumusb_bluetoothbeacon_library.altbeacon.beacon.BeaconController;
import com.qn.quantumusb_bluetoothbeacon_library.altbeacon.beacon.Beacon_Model;

public class MainActivity extends AppCompatActivity implements Beacon_Model {

    BeaconController Beacon_controll;
    public static final String MESSAGE_TO_PRINT = "hello world hp printer";
    Button btn_start,btn_stop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_start=findViewById(R.id.btn_start);
        btn_stop=findViewById(R.id.btn_stop);




        Beacon_controll = new BeaconController(new MainActivity(),MainActivity.this,"f06a8c12f45f80721e5e4777bd8c5df1",MainActivity.class,true);

        // hpBeaconController.print(MESSAGE_TO_PRINT);

        getBEaconData();

        btn_stop.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                Beacon_controll.Stop_Scan_Beacons();
            }
        });
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Beacon_controll = new BeaconController(new MainActivity(),MainActivity.this,"f06a8c12f45f80721e5e4777bd8c5df1",MainActivity.class,true);
            }
        });


    }


    public void getBEaconData()
    {
        String data_return=Beacon_controll.Beacon_Detail(MESSAGE_TO_PRINT);
        Log.d("Call_InterFace",""+data_return);
    }


    @Override
    public String Beacon_Detail(String message)
    {
        Log.d("Res_main_class",""+message);
        return null;
    }
}