package com.qsmplibrary.altbeacon.beacon

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.qn.quantumusb_bluetoothbeacon_library.altbeacon.beacon.Beacon

class RegionViewModel: ViewModel() {
    val regionState: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }
    val rangedBeacons: MutableLiveData<Collection<Beacon>> by lazy {
        MutableLiveData<Collection<Beacon>>()
    }
}