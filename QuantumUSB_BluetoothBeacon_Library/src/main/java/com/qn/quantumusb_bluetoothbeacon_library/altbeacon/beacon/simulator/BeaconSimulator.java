package com.qn.quantumusb_bluetoothbeacon_library.altbeacon.beacon.simulator;



import com.qn.quantumusb_bluetoothbeacon_library.altbeacon.beacon.Beacon;

import java.util.List;

/**
 * Created by dyoung on 4/18/14.
 */
public interface BeaconSimulator {
    public List<Beacon> getBeacons();
}
