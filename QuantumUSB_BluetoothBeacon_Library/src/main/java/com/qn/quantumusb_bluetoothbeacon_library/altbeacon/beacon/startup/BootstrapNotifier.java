package com.qn.quantumusb_bluetoothbeacon_library.altbeacon.beacon.startup;

import android.content.Context;

import com.qn.quantumusb_bluetoothbeacon_library.altbeacon.beacon.MonitorNotifier;


/**
 * @deprecated Will be removed in 3.0.  See http://altbeacon.github.io/android-beacon-library/autobind.html
 */
@Deprecated
public interface BootstrapNotifier extends MonitorNotifier {
    public Context getApplicationContext();
}
