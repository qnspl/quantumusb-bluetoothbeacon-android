package com.qn.quantumusb_bluetoothbeacon_library.altbeacon.beacon;

public interface Beacon_Model
{
    // This is just a regular method so it can return something or
    // take arguments if you like.
    String Beacon_Detail(final String message);
}